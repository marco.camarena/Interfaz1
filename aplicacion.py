from tkinter import *
from received import *
from play import *
import queue
import time

class Aplicacion(object):
	def __init__(self, menv, mrec):
		self.principal = Tk()
		self.principal.geometry('800x600')
		self.principal.title('FXDP')
		self.principal.resizable(0,0)

		self.menv = menv
		self.mrec = mrec
		
		#DEFINICION IMAGENES
		syncImage = PhotoImage(file = 'sync.png')
		openImage = PhotoImage(file = 'open.png')
		saveImage = PhotoImage(file = 'save.png')
		stopImage = PhotoImage(file = 'stop.png')
		pauseImage = PhotoImage(file = 'pause.png')
		playImage = PhotoImage(file = 'play.png')
		
		#DEFINICION DE WIDGETS
		self.syncButton = Button(self.principal, image = syncImage, width = 100, height = 50, command = self.sync)
		self.openButton = Button(self.principal, image = openImage, width = 50, height = 50)
		self.saveButton = Button(self.principal, image = saveImage, width = 50, height = 50)
		self.stopButton = Button(self.principal, image = stopImage, width = 50, height = 50, command = self.stop)
		self.pauseButton = Button(self.principal, image = pauseImage, width = 50, height = 50)
		self.playButton = Button(self.principal, image = playImage, width = 75, height = 50, command = self.play)

		self.dispositivosLabel = Label(self.principal, text = "DISPOSITIVOS", font = 'bold 11 bold')	
		self.comandosLabel = Label(self.principal, text = "COMANDOS", font = 'bold 11 bold')

		self.frameDispositivos = Frame(self.principal, width = 250, height = 300)
		self.dispositivos = Text(self.frameDispositivos, width = 35, height = 21)

		self.frameConsola = Frame(self.principal, width = 250, height = 100)
		self.consola = Text(self.frameConsola, width = 35, height = 7)

		self.frameComandos = Frame(self.principal, width = 450, height = 425)
		self.comandos = Text(self.frameComandos, width = 64, height = 30)

		#DEFINICION DE UBICACIONES DE WIDGETS
		self.syncButton.place(x=100, y=25)
		self.openButton.place(x=325, y=25)
		self.saveButton.place(x=400, y=25)
		self.stopButton.place(x=550, y=25)
		self.pauseButton.place(x=625, y=25)
		self.playButton.place(x=700, y=25)

		self.dispositivosLabel.place(x=80, y=110)
		self.comandosLabel.place(x=500, y=110)

		self.frameDispositivos.place(x=25, y=150)
		self.dispositivos.place(x=0, y=0)

		self.frameConsola.place(x=25, y=475)
		self.consola.place(x=0, y=0)

		self.frameComandos.place(x=325, y=150)
		self.comandos.place(x=0, y=0)

		self.principal.mainloop()

	def sync(self):
		mensajes = Received(self.menv, self.mrec, self.dispositivos, self.consola)
		self.menv.put("DD")

	def stop(self):
		self.menv.put("SA -p 12 -o L")

	def play(self):
		playHilo = Play(self.menv, self.consola, self.comandos)
