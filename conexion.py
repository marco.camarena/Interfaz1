import socket
import threading

class Conexion(threading.Thread):

	def __init__(self, menv, mrec):
		threading.Thread.__init__(self)
		self.menv = menv
		self.mrec = mrec

	def run(self):
		host = '192.168.1.10'
		port = 4444

		client = socket.socket()
		client.connect((host,port))

		while True:
			try:
				data = client.recv(1024).decode()
				self.mrec.put(data)
				print ('Received: ' + data)

				x = self.menv.get()
				if "" != x:
					print("SEND:", x)
					x = x + '\n'
					client.send(x.encode('utf-8'))
					if "bye\n" == x:
						print("Shutting down.")
						break

			except KeyboardInterrupt as k:
				print("Shutting down.")
				client.close()
				break
