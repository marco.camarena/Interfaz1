from aplicacion import *
from conexion import *
import queue

menv = queue.Queue()
mrec = queue.Queue()

def main():
	conexion = Conexion(menv, mrec)
	conexion.start()

	mi_app = Aplicacion(menv, mrec)

	return 0


if __name__ == '__main__':
	main()