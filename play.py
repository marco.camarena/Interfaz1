import threading
from tkinter import *
import queue
import time

class Play(threading.Thread):
	def __init__(self, menv, consola, comandos):
		threading.Thread.__init__(self)
		self.menv = menv
		self.consola = consola
		self.comandos = comandos
		self.start()

	def run(self):
		while True:
			lineas = self.comandos.get(1.0, END)
			colaComandos = queue.Queue()
			i = 0
			aux = ""
			longitud = len(lineas)
			while i < longitud:
				if lineas[i] != '\n':
					aux = aux + lineas[i]
				else:
					colaComandos.put(aux)
					aux = ""
				i = i+1

			while True:
				if colaComandos.empty() == True:
					break;
				self.menv.put(colaComandos.get())
				time.sleep(1)
			break;
